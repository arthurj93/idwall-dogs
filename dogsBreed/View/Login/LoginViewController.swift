//
//  LoginViewController.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 25/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class LoginViewController: UIViewController, NVActivityIndicatorViewable {
    
    enum Constants {
        static let loginButtonCornerRadius: CGFloat = 5
    }
    
    let network = APIManager.shared()
    let loginRealm = LoginDataImplementation.shared()

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    func setupView() {
        emailTextField.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
        loginButton.isEnabled = false
        loginButton.backgroundColor = getColor(ColorString.blueLogoLowOpacity)
        loginButton.layer.cornerRadius = Constants.loginButtonCornerRadius
    }
    
    @IBAction func login(_ sender: Any) {
        
        guard let email = emailTextField.text else { return }
        
        login(email: email)
        
    }
    
    @objc func handleTextInputChange() {
        
        guard let isFormValid = emailTextField.text?.isValidEmail else { return }
        
        if isFormValid {
            loginButton.isEnabled = true
            loginButton.backgroundColor = getColor(ColorString.blueLogo)
        } else {
            loginButton.isEnabled = false
            loginButton.backgroundColor = getColor(ColorString.blueLogoLowOpacity)
        }
        
    }

}



extension LoginViewController: handleErrorAPI {
    
    func handleError(_ error: Error) {
        guard let err = error as? Network.Error else {
            log("Unhandled error: \(error.localizedDescription)", event: .error)
            return
        }
        
        switch err {
        case let .api(_, response):
            if response.errors.isEmpty == false {
                response.errors.forEach(validaFieldWithWebServiceErrors(_:))
            } else {
                print(response.message)
                showError(AppStrings.common_alert_warning_title, message: response.message)
            }
            
        case .notConnectedToInternet:
            showError(AppStrings.common_alert_error_title, message: AppStrings.apiError_noInternetConnection)
            
        case let .http(error):
            showError(AppStrings.common_alert_error_title, message: error.localizedDescription)
            log("Unhandled error: \(error.localizedDescription)", event: .error)
        }
        
        self.stopAnimating()
    }
    
    func validaFieldWithWebServiceErrors(_ error: Network.ErrorResponse.Item) {
        print(error.messages.first)
    }
    
    func login(email: String) {
        startAnimating()
        network.request(API.Login.login(email: email)) { (response: UserResponse?, error: Error?) in
            if let error = error{
                self.handleError(error)
            }
            
            if let userResponse = response {
                self.loginRealm.saveUser(email: email, token: userResponse.user.token)
                let layout = UICollectionViewFlowLayout()
                let listBreedsVC = ListBreedsCollectionViewController(collectionViewLayout: layout, email: email)
                self.present(UINavigationController(rootViewController: listBreedsVC), animated: true, completion: nil)
                self.stopAnimating()
            }
        }
    }
}
