//
//  ListBreedCell.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 12/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import UIKit

class ListBreedCell: UICollectionViewCell {
    
    enum Constanst {
        static let cornerRadius:CGFloat = 5
    }
    
    static let cellId = "ListBreedCell"

    @IBOutlet weak var nameBreedCell: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageBreed: UIImageView!
    
    func setup(breed: BreedLocal) {
        nameBreedCell.text = breed.name
        nameBreedCell.textColor = .white
        imageBreed.image = breed.image
        containerView.layer.cornerRadius = Constanst.cornerRadius
        containerView.backgroundColor = getColor(ColorString.blueLogoLowOpacity)
    }

}
