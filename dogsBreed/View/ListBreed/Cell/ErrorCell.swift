//
//  ErrorCell.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 25/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import UIKit

class ErrorCell: UICollectionViewCell {

    enum Constanst {
        static let cornerRadius:CGFloat = 5
    }
    
    static let cellId = "ErrorCell"
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    func setup(error: String) {
        errorLabel.text = error
        errorLabel.textColor = .white
        containerView.layer.cornerRadius = Constanst.cornerRadius
        containerView.backgroundColor = getColor(ColorString.blueLogoLowOpacity)
    }

}
