//
//  BreedImagesCollectionViewController.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 12/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import RealmSwift

class BreedImagesCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, NVActivityIndicatorViewable {

    enum Constants {
        static let widthDivisor: CGFloat = 3
        static let padding: CGFloat = 2
        static let itemSpacing: CGFloat = 0
        static let lineSpacing: CGFloat = 2
        static let countImagesLimit = 4
        static let cellCount = 1
        static let paddingListCell: CGFloat = 32
        static let cellHeight: CGFloat = 50
        static let collectionViewTopPadding: CGFloat = 16
    }
    
    let network = APIManager.shared()
    let loginRealm = LoginDataImplementation.shared()
    
    var breedStrings: [String] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    let email: String
    
    let breedName: String
    
    init(collectionViewLayout layout: UICollectionViewLayout,
         breedName: String,
         email: String) {
        self.breedName = breedName
        self.email = email
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(BreedImageCell.self, forCellWithReuseIdentifier: BreedImageCell.cellId)
        self.collectionView.register(UINib(nibName: ErrorCell.cellId, bundle: nil), forCellWithReuseIdentifier: ErrorCell.cellId)
        
        collectionView?.backgroundColor = getColor(ColorString.grayBackground)
        collectionView?.contentInset = .init(top: Constants.collectionViewTopPadding, left: 0, bottom: 0, right: 0)
        collectionView?.scrollIndicatorInsets = .init(top: Constants.collectionViewTopPadding, left: 0, bottom: 0, right: 0)
        
        setupNavigationBar(title: breedName, titleColor: .white, barTintColor: getColor(ColorString.blueLogoLowOpacity))
        getImagesBy(breed: breedName.lowercased())
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if breedStrings.count == 0 {
            return Constants.cellCount
        }
        
        return breedStrings.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if breedStrings.count == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ErrorCell.cellId, for: indexPath) as! ErrorCell
            cell.setup(error: AppStrings.apiError_refresher)
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BreedImageCell.cellId, for: indexPath) as! BreedImageCell
        
        cell.breedImage = breedStrings[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if breedStrings.count == 0 {
            return CGSize(width: collectionView.bounds.width - Constants.paddingListCell, height: Constants.cellHeight)
        }
        
        return CGSize(width: view.frame.width/Constants.widthDivisor - Constants.padding, height: view.frame.width/Constants.widthDivisor - Constants.padding)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.itemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.lineSpacing
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if breedStrings.count != 0 {
            let url = breedStrings[indexPath.item]
            let breedImageDetailController = BreedImageDetailViewController()
            breedImageDetailController.breedImage = url
            self.navigationController?.pushViewController(breedImageDetailController, animated: true)
            return
        }
        
        getImagesBy(breed: breedName.lowercased())
    }
    
}

extension BreedImagesCollectionViewController: handleErrorAPI {
    
    func handleError(_ error: Error) {
        guard let err = error as? Network.Error else {
            log("Unhandled error: \(error.localizedDescription)", event: .error)
            return
        }
        
        switch err {
        case let .api(_, response):
            if response.errors.isEmpty == false {
                response.errors.forEach(validaFieldWithWebServiceErrors(_:))
            } else {
                print(response.message)
                showError(AppStrings.common_alert_warning_title, message: response.message)
            }
            
        case .notConnectedToInternet:
            showError(AppStrings.common_alert_error_title, message: AppStrings.apiError_noInternetConnection)
            
        case let .http(error):
            showError(AppStrings.common_alert_error_title, message: error.localizedDescription)
            log("Unhandled error: \(error.localizedDescription)", event: .error)
        }
        
        self.stopAnimating()
    }
    
    func validaFieldWithWebServiceErrors(_ error: Network.ErrorResponse.Item) {
        print(error.messages.first)
    }
    
    func getImagesBy(breed: String) {
        startAnimating()
        let token = loginRealm.getToken(email: email)
        network.request(API.Breed.getFeed(breed: breedName, token: token)) { (response: FeedResponse?, error: Error?) in
            if let error = error{
                self.handleError(error)
            }
            
            if let feedResponse = response {
                self.breedStrings = feedResponse.list
                self.stopAnimating()
            }
        }
        
    }
}
