//
//  ListBreedsCollectionViewController.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 12/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ListBreedsCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    enum Constants {
        static let lineSpacing: CGFloat = 2
        static let itemSpacing: CGFloat = 0
        static let padding: CGFloat = 32
        static let cellPadding: CGFloat = 2
        static let collectionViewTopPadding: CGFloat = 16
        static let widthDivisor: CGFloat = 2
    }
    
    init(collectionViewLayout layout: UICollectionViewLayout, email: String) {
        self.email = email
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let breedList: [BreedLocal] = [BreedLocal(name: "Husky", image: #imageLiteral(resourceName: "husky")),
                             BreedLocal(name: "Pug", image: #imageLiteral(resourceName: "pug")),
                             BreedLocal(name: "Labrador", image: #imageLiteral(resourceName: "labrador")),
                             BreedLocal(name: "Hound", image: #imageLiteral(resourceName: "hound"))]
    
    let email: String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = getColor(ColorString.grayBackground)
        view.backgroundColor = getColor(ColorString.grayBackground)
        collectionView.register(UINib(nibName: ListBreedCell.cellId, bundle: nil), forCellWithReuseIdentifier: ListBreedCell.cellId)
        collectionView?.contentInset = .init(top: Constants.collectionViewTopPadding, left: 0, bottom: 0, right: 0)
        collectionView?.scrollIndicatorInsets = .init(top: Constants.collectionViewTopPadding, left: 0, bottom: 0, right: 0)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                              leading: view.safeAreaLayoutGuide.leadingAnchor,
                              bottom: view.safeAreaLayoutGuide.bottomAnchor,
                              trailing: view.safeAreaLayoutGuide.trailingAnchor)
        setupNavigationBar(title: AppStrings.common_app_name, titleColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), barTintColor: UIColor(named: "blueLogoLowOpacity")!)
        navigationController?.navigationBar.tintColor = .white
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return breedList.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ListBreedCell.cellId, for: indexPath) as! ListBreedCell
        
        cell.setup(breed: breedList[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width/Constants.widthDivisor - Constants.cellPadding, height: collectionView.bounds.width/Constants.widthDivisor - Constants.cellPadding)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let breedName = breedList[indexPath.item].name
        let layout = UICollectionViewFlowLayout()
        let breedImageController = BreedImagesCollectionViewController(collectionViewLayout: layout,
                                                                       breedName: breedName,
                                                                       email: email)
        
        self.navigationController?.pushViewController(breedImageController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.lineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.itemSpacing
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        DispatchQueue.main.async {
            self.collectionView.collectionViewLayout.invalidateLayout()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
}
