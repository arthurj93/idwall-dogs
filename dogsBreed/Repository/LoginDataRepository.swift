//
//  LoginDataRepository.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 25/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import Foundation
import RealmSwift


protocol LoginData {
    func saveUser(email: String, token: String)
    func getToken(email: String) -> String
}

class LoginDataImplementation: LoginData {
    
    private var realm: Realm

    private init(realm: Realm) {
        self.realm = realm
    }

    private static var shareBreedData: LoginDataImplementation = {
        let breedData = LoginDataImplementation(realm: try! Realm())
        return breedData
    }()

    class func shared() -> LoginDataImplementation {
        return shareBreedData
    }
    
    func saveUser(email: String, token: String) {
        let user = RMLogin(value: ["email": email, "token": token])
        
        do {
            try realm.write {
                realm.add(user, update: true)
            }
        } catch let realmErr {
            print(realmErr)
        }
    }
    
    func getToken(email: String) -> String {
        let user = realm.object(ofType: RMLogin.self, forPrimaryKey: email)
        
        if let unwrappedLogin = user {
            return LoginConverter.convertObject(obj: unwrappedLogin).token
        }
        return ""
    }
}
