//
//  LoginConverter.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 25/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import Foundation

class LoginConverter {

    static func convertObject(obj: RMLogin) -> Login {

        let user =  Login(email: obj.email, token: obj.token)

        return user
    }

    static func convertObjects(objs: [RMLogin]) -> [Login] {

        var user = [Login]()

        for obj in objs {
            user.append(convertObject(obj: obj))
        }
        return user
    }
}
