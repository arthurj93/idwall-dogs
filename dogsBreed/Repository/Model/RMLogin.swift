//
//  RMLogin.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 25/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import RealmSwift


class RMLogin: Object {
    @objc dynamic var email = ""
    @objc dynamic var token = ""
    
    override class func primaryKey() -> String? {
        return "email"
    }
}

