//
//  File.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 25/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import UIKit

struct Login: Codable {
    let email: String
    let token: String
}
