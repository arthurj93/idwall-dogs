//
//  Breeds.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 13/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import UIKit

struct BreedLocal {
    let name: String
    let image: UIImage
}
