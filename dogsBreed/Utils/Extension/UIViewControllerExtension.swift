//
//  UIViewControllerExtension.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 12/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import UIKit


extension UIViewController {
    
    func showError(_ title: String?, message: String?) {
        UIAlertController.presentAlert(
            in: self,
            title: title,
            message: message,
            actions: [UIAlertAction(title: AppStrings.common_ok, style: .default, handler: nil)]
        )
    }
    
    func setupNavigationBar(title: String?, titleColor: UIColor, barTintColor: UIColor) {
        self.navigationItem.title = title
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.isTranslucent = false
        let attributes = [NSAttributedString.Key.foregroundColor: titleColor]
        navigationController?.navigationBar.titleTextAttributes = attributes
    }
}
