//
//  StringExtension.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 25/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import Foundation

extension String {
    
    var isValidEmail: Bool {
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: self)
    }

}


