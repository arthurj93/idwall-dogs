//
//  FeedResponse.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 25/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import Foundation

struct FeedResponse: Codable {
    let category: String
    let list: [String]
}
