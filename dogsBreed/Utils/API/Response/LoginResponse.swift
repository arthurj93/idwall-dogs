//
//  LoginResponse.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 25/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import UIKit

struct LoginResponse: Codable {
    
    let _id: String
    let email: String
    let token: String
    let createdAt: String
    let updatedAt: String
    let __v: Int
}

struct UserResponse: Codable {
    let user: LoginResponse
}

