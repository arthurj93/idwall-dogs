//
//  EndPoints.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 12/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import Foundation
import Alamofire

struct Endpoint: NetworkingResource {
    
    static var baseURL: URL { return API.baseURL }
    
    let path: String
    let method: HTTPMethod
    let body: Parameters?
    let query: Parameters?
    let encoding: ParameterEncoding
    let headers: HTTPHeaders?
    
    init(path: String,
         method: HTTPMethod = .get,
         body: Parameters? = nil,
         query: Parameters? = nil,
         headers: HTTPHeaders? = nil,
         encoding: ParameterEncoding = URLEncoding.default) {
        self.path = path
        self.method = method
        self.body = body
        self.query = query
        self.headers = headers
        self.encoding = encoding
    }
    
}

enum API {
    static let baseURL = URL(string: "https://api-iddog.idwall.co")!
    
    enum Login {
        
        static func login(email: String) -> NetworkingResource {
            
            return Endpoint(path: "/signup",
                            method: .post,
                            body: ["email": email],
                            encoding: JSONEncoding.default)
        }
        
    }
    
    enum Breed {
        
        static func getFeed(breed: String, token: String) -> NetworkingResource {
        
            return Endpoint(path: "/feed",
                            method: .get,
                            query: ["category": breed.lowercased()],
                            headers: ["Authorization": token],
                            encoding: JSONEncoding.default)
            
            
            
        }
    }
}

