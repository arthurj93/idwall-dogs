//
//  ColorString.swift
//  dogsBreed
//
//  Created by Arthur Jatoba on 25/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import UIKit

func getColor(_ name: String) -> UIColor {
    return UIColor(named: name)!
}

enum ColorString {
    static let blueLogo = "blueLogo"
    static let blueLogoLowOpacity = "blueLogoLowOpacity"
    static let grayBackground = "grayBackground"
}
