//
//  dogsBreedTests.swift
//  dogsBreedTests
//
//  Created by Arthur Jatoba on 13/02/19.
//  Copyright © 2019 Arthur Jatoba. All rights reserved.
//

import XCTest
import RealmSwift
@testable import dogsBreed

class dogsBreedTests: XCTestCase {
    
    
    let realm = try! Realm()
    let loginRealm = LoginDataImplementation.shared()

    override func setUp() {
        
        try! realm.write {
            realm.deleteAll()
        }
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    func testSaveUser() {
        
        let userMock = Login(email: "email@email.com", token: "123456")
        
        let loginMock = Login(email: userMock.email, token: userMock.token)
        
        loginRealm.saveUser(email: loginMock.email, token: loginMock.token)
        
        let token = loginRealm.getToken(email: userMock.email)
        
        XCTAssertEqual(userMock.token, token)
        
    }
    
    func testLoginGetTokenWithZeroUsers() {
        let userMock = Login(email: "email@email.com", token: "123456")
        let token = loginRealm.getToken(email: userMock.email)
        
        XCTAssertEqual(token, "")
    }
}
