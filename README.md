# IDwall-iddog
Desafio IDwall-iddog

# Instalação
1. Clonar repositório
2. Abrir terminal na pasta do projeto e usar pod install
4. Abrir workspace e rodar `IDwall-iddog` no seu dispositivo/simulador

# Requisitos
* iOS 12.0+
* Xcode 10.0+
* Swift 4.2+

# Bibliotecas
1. 'IQKeyboardManager', teclado customizado impedindo que o teclado cubra os TextFields/TextsViews
2. 'Alamofire', fazer requests para API
3. 'NVActivityIndicatorView', loader customizável com animações e travamento de UI
4. 'SDWebImage', fazer downloads de imagens assincronamente e cache
5. 'RealmSwift', banco de dados local

# Arquitetura
* MVC
